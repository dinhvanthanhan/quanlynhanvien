const DSNV = "DSNV";
var dsnv = [];



function themNv() {
    var newNv = layThongTinTuForm();
    var isValid = true;

    // kiem tra tai khoan
    isValid = isValid & kiemTraRong(newNv.account, "tbTKNV") &&
        kiemTraTaiKhoan(newNv.account, "tbTKNV");

    // kiem tra ten
    isValid = isValid & kiemTraRong(newNv.name, "tbTen") &&
        kiemTraTen(newNv.name, "tbTen");

    // kiem tra email
    isValid = isValid & kiemTraRong(newNv.email, "tbEmail") &&
        kiemTraEmail(newNv.email, "tbEmail");

    // kiem tra mat khau
    isValid = isValid & kiemTraRong(newNv.password, "tbMatKhau") &&
        kiemTraMatKhau(newNv.password, "tbMatKhau");

    // kiem tra luong cb
    isValid = isValid & kiemTraRong(newNv.basicSalary, "tbLuongCB") &&
        kiemTraLuongCB(newNv.basicSalary, "tbLuongCB");

    // kiem tra chuc vu
    isValid = isValid & kiemTraRong(newNv.position, "tbChucVu") &&
        kiemTraChucVu(newNv.position, "tbChucVu");

    // kiem tra gio lam viec 
    isValid = isValid & kiemTraRong(newNv.hour, "tbGiolam") &&
        kiemTraGio(newNv.hour, "tbGiolam");

    
    //     kiemTraRong(newNv.startDay, "tbNgay") &

    if (isValid) {
        dsnv.push(newNv);
        renderDsnv(dsnv);
        
    } else {
        console.log("no");
    }
    
}

function xoaNv(name) {
    var name = dsnv.indexOf(function (nv) {
        return nv.name == name
    });

    dsnv.splice(name, 1);

    renderDsnv(dsnv);
}

function suaSv(name) {
    var name = dsnv.indexOf(function (nv) {
        return nv.name == name
    });

    var nv = dsnv[name];

    showThongTinLenForm(nv);

}

function timLoaiNhanVien() {
    var type = document.getElementById("searchName").value;
    var newDsnv = [];
    if (type == "Xuất sắc") {
        for (let i = 0; i < dsnv.length; i++) {
            var currentNv = dsnv[i];
            if (currentNv.type() == "Xuất sắc") {
                newDsnv.push(currentNv);
            }   
        }
        renderDsnv(newDsnv);
    } else if (type == "Giỏi") {
        for (let i = 0; i < dsnv.length; i++) {
            var currentNv = dsnv[i];
            if (currentNv.type() == "Giỏi") {
                newDsnv.push(currentNv);
            }   
        }
        renderDsnv(newDsnv);
    } else if (type == "Khá") {
        for (let i = 0; i < dsnv.length; i++) {
            var currentNv = dsnv[i];
            if (currentNv.type() == "Khá") {
                newDsnv.push(currentNv);
            }   
        }
        renderDsnv(newDsnv);
    } else if (type == "Trung bình") {
        for (let i = 0; i < dsnv.length; i++) {
            var currentNv = dsnv[i];
            if (currentNv.type() == "Trung bình") {
                newDsnv.push(currentNv);
            }   
        }
        renderDsnv(newDsnv);
    } else {
        document.getElementById("tbLoaiNv").innerHTML = "Không có loại nhân viên bạn đang tìm"
    }
    
}