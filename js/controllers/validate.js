function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText =
      "Trường này không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraTaiKhoan(value, idError) {
  if (value.length < 4 || value.length > 6) {
    document.getElementById(idError).innerHTML =
      "Tài khoản phải từ 4-6 kí số";
    console.log("hi");
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraTen(value, idError) {
  var letters = /^[A-Za-z]+$/;
  var isName = letters.test(value);

  if (!isName) {
    document.getElementById(idError).innerText = "Tên không được có số";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraMatKhau(value, idError) {
  if (value.length < 6 || value.length > 10) {
    document.getElementById(idError).innerText =
      "Mật khẩu phải từ 6-10 kí tự";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraChucVu(value, idError) {
  if (value != 1 && value != 2 && value != 3) {
    document.getElementById(idError).innerText =
      "Vui lòng chọn chức vụ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraLuongCB(value, idError) {
  if (value < 1000000 || value > 20000000) {
    document.getElementById(idError).innerText =
      "Lương cơ bản phải từ 1,000,000 tới 20,000,000";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraGio(value, idError) {
  if (value < 80 || value > 200) {
    document.getElementById(idError).innerText =
      "Giờ làm việc trong tháng phải từ 80 tới 200";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}