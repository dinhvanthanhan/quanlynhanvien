function layThongTinTuForm() {
    var account = document.getElementById("tknv").value.trim();
    var name = document.getElementById("name").value.trim();
    var email = document.getElementById("email").value.trim();
    var password = document.getElementById("password").value.trim();
    var startDay = document.getElementById("datepicker").value.trim();
    var basicSalary = document.getElementById("luongCB").value.trim();
    var position = document.getElementById("chucvu").value.trim();
    var hour = document.getElementById("gioLam").value.trim();

    var nv = new NhanVien(account, name, email, password, startDay, basicSalary, position, hour);
    return nv;

}

function renderDsnv(dsnv) {
    var contentHTML = "";
    for (var i = 0; i < dsnv.length; i++) {
        var currentNv = dsnv[i];
        var contentTr = `<tr>
                            <td>${currentNv.account}</td>
                            <td>${currentNv.name}</td>
                            <td>${currentNv.email}</td>
                            <td>${currentNv.startDay}</td>
                            <td>${currentNv.position}</td>
                            <td>${currentNv.salary()}</td>
                            <td>${currentNv.type()}</td>
                            <td>
                                <button class="btn btn-danger" onclick="xoaNv()">Xóa</button>
                                <button class="btn btn-primary" onclick"suaNv()" data-toggle="modal"
                                data-target="#myModal">Sửa</button>
                            </td>
                        </tr>`;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nhanVien) {
    document.getElementById("tknv").value = nhanVien.account;
    document.getElementById("name").value = nhanVien.name;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.pasword;
    document.getElementById("datepicker").value = nhanVien.startDay;
    document.getElementById("luongCB").value = nhanVien.basicSalary;
    document.getElementById("chucvu").value = nhanVien.position;
    document.getElementById("gioLam").value = nhanVien.hour;

  }

