function NhanVien(account, name, email, password, startDay, basicSalary, position, hour) {
    this.account = account;
    this.name = name;
    this.email = email;
    this.password = password;
    this.startDay = startDay;
    this.basicSalary = basicSalary;
    this.position = position;
    this.hour = hour;
    this.salary = function() {
        var salary = this.basicSalary * 1 * this.position * 1;
        return salary;
    };
    this.type = function() {
        if (this.hour * 1 < 160 && this.hour * 1 > 0) {
            return "Trung bình";
        } else if (this.hour * 1 >= 160 && this.hour *1 < 176) {
            return "Khá";
        } else if (this.hour * 1 >= 176 && this.hour * 1 < 192) {
            return "Giỏi"
        } else {
            return "Xuất sắc";
        }
    };

}